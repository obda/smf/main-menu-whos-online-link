[size=x-large][b]Main Menu “Who’s Online” Link[/b][/size]

A modification for [url=https://www.simplemachines.org]Simple Machines Forum (SMF) 2.1[/url] that adds a link to the “Who’s Online” page to the main menu.

This modification does not change any stored data.  It merely adapts the generated output via hooks.


[size=large][b]Settings[/b][/size]

There are no settings for this mod.  Uninstall it to disable its functionality.


[size=large][b]License[/b][/size]

Main Menu “Who’s Online” Link is released under the BSD-3-Clause license.


[size=large][b]Changelog[/b][/size]

Version 1.0.0 (2024-06-17):
[list]
[li]First public release.[/li]
[/list]


[size=large][b]Repository[/b][/size]

https://gitlab.com/obda/smf/main-menu-whos-online-link
