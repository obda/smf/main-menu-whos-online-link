<?php

namespace obda\MainMenuWhosOnlineLink;

/**
 * The Main Menu “Who’s Online” Link modification.
 */
class MainMenuWhosOnlineLink
{
    /**
     * The copyright notice for this SMF mod.
     */
    public const COPYRIGHT = "Main Menu “Who’s Online” Link " .
                             "by obda Technologies, © 2024";

    /**
     * Initialize a new instance of this class.
     *
     * @param object $smf An object with a public `allowedTo()` method that
     *                    checks if the currently logged in user has one (or
     *                    more) given permissions.  If omitted, will use an
     *                    ad-hoc class that forwards calls to `\allowedTo()`
     *                    in the root namespace.
     */
    public function __construct(?object $smf = null)
    {
        if (is_null($smf)) {
            $wrapper = new class {
                public function allowedTo($permission, ...$args): bool
                {
                    return \allowedTo($permission, ...$args);
                }
            };
            $smf = new $wrapper();
        }
        $this->smf = $smf;
    }

    /**
     * Add a link to “Who’s Online” to the main menu, right after “Search”.
     *
     * @param array $buttons The list of menu buttons, which will be modified
     *                       inline.
     */
    public function addMenuItem(array &$buttons): void
    {
        global $txt, $scripturl, $context, $modSettings;

        # Find the insert position in the `$buttons` array
        $insert_pos = array_search("search", array_keys($buttons), true) + 1;

        # Define the new menu item
        $new_menu = [
            "who" => [
                "title" => $txt["who_title"],
                "href" => $scripturl . "?action=who",
                "show" => (
                    $this->smf->allowedTo("who_view")
                    && !empty($modSettings["who_enabled"])
                ),
                "sub_buttons" => [],
                "icon" => "time_online",
            ]
        ];

        # Insert the new item into the existing array
        $buttons = array_merge(
            array_slice($buttons, 0, $insert_pos),
            array_merge($new_menu, array_slice($buttons, $insert_pos))
        );
    }

    /**
     * Add credits for this modification to the global context.
     */
    public function credits(): void
    {
        global $context;
        $context["copyrights"]["mods"][] = $this::COPYRIGHT;
    }
}
