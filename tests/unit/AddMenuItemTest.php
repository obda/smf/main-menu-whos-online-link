<?php

declare(strict_types=1);

namespace obda\MainMenuWhosOnlineLink;

use PHPUnit\Framework\TestCase;

/**
 * Tests for the `MainMenuWhosOnlineLink::addMenuItem()` function.
 */
final class AddMenuItemTest extends TestCase
{
    /**
     * Set up common test functionality.
     *
     * This sets up the data structure for a dummy menu bar, a mock for SMF’s
     * `allowedTo()` permission checking function, and an instance of this
     * SMF modification.
     */
    protected function setUp(): void
    {
        $this->menu = ["home" => [], "search" => [], "admin" => []];
        $this->smf = $this->getMockBuilder(stdClass::class)
                          ->setMethods(["allowedTo"])
                          ->getMock();
        $this->mod = new MainMenuWhosOnlineLink($this->smf);
    }

    /**
     * Verify that the menu item to “Who’s Online” is inserted after “Search”.
     */
    public function testCorrectPosition(): void
    {
        $this->mod->addMenuItem($this->menu);
        $expected_keys = ["home", "search", "who", "admin"];
        $this->assertSame($expected_keys, array_keys($this->menu));
    }

    /**
     * Return all potential variations for the `testShowFlag()` test.
     *
     * @return array[] A list of boolean triples.  The first boolean indicates
     *                 whether the “Who’s Online” view has been enabled in the
     *                 global SMF configuration, and the second one denotes if
     *                 the currently logged in user has the permissions to
     *                 access the “Who’s Online” view.  The third boolean
     *                 contains the expected result as a combination of the
     *                 previous two values, whether the additional menu item
     *                 shall be shown or not.
     */
    public static function showFlagProvider(): array
    {
        return [
            [true, true, true],
            [true, false, false],
            [false, true, false],
            [false, false, false],
        ];
    }

    /**
     * Check whether the new menu item will actually be shown to the user.
     *
     * For this to be the case, the “Who’s Online” view must be enabled in the
     * global SMF configuration, and, additionally, the currently logged in
     * user also needs to have appropriate permissions.
     *
     * @param bool $is_enabled A flag that indicates whether the “Who’s
     *                         Online” view has been enabled in the global SMF
     *                         configuration.
     * @param bool $is_allowed A flag that represents if the currently logged
     *                         in user has permission to access the “Who’s
     *                         Online” view.
     * @param bool $expected The expected result whether the new menu item
     *                       shall be shown to the user.
     *
     * @dataProvider showFlagProvider
     */
    public function testShowFlag(
        bool $is_enabled,
        bool $is_allowed,
        bool $expected
    ): void {
        global $modSettings;
        $modSettings["who_enabled"] = $is_enabled;
        $this->smf->method("allowedTo")->willReturn($is_allowed);
        $this->mod->addMenuItem($this->menu);
        $this->assertSame($expected, $this->menu["who"]["show"]);
    }
}
