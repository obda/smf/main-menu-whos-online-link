Main Menu “Who’s Online” Link
=============================

A modification for [Simple Machines Forum (SMF) 2.1][1] that adds a link to
the “Who’s Online” page to the main menu.

This modification does not change any stored data.  It merely adapts the
generated output via hooks.

[1]: https://www.simplemachines.org
